// console.log(React);
// console.log(ReactDOM);

var my_news = [
    {
        author: 'Саша Печкин',
        text: 'В четчерг, четвертого числа...',
        bigText: 'в четыре с четвертью часа четыре чёрненьких чумазеньких чертёнка чертили чёрными чернилами чертёж.'
    },
    {
        author: 'Просто Вася',
            text: 'Считаю, что $ должен стоить 35 рублей!',
        bigText: 'А евро 42!'
    },
    {
        author: 'Гость',
            text: 'Бесплатно. Скачать. Лучший сайт - http://localhost:3000',
        bigText: 'На самом деле платно, просто нужно прочитать очень длинное лицензионное соглашение'
    }
];


var Add = React.createClass({

    componentDidMount: function () {  //Выполнение действий при загрузке страницы
        // console.log( document.body.getElementsByClassName("add_author"));
        ReactDOM.findDOMNode(this.refs.author).focus(); //Установка фокуса на авторе (первое поле)
    },

    onButtonClickHandler: function (e) {  //Вывод значения author по нажатию кнопки
        e.preventDefault();
        // console.log(this.refs);
        var str = ReactDOM.findDOMNode(this.refs.author).value + "\n\n" + ReactDOM.findDOMNode(this.refs.text).value;
        alert(str);
    },

    getInitialState: function () {  //Установка начального состояния
        return {
            agreeNotChecked: true,
            authorIsEmpty: true,
            textIsEmpty: true
        }
    },


    onCheckRuleClick: function(e) {
        this.setState({agreeNotChecked: !this.state.agreeNotChecked});
    },
    // onAuthorChange: function(e) {  //Проверка введенного текста
    //     if (e.target.value.trim().length > 0) {
    //         this.setState({authorIsEmpty: false})
    //     } else {
    //         this.setState({authorIsEmpty: true})
    //     }
    // },
    // onTextChange: function(e) {  //Проверка введенного текста
    //     if (e.target.value.trim().length > 0) {
    //         this.setState({textIsEmpty: false})
    //     } else {
    //         this.setState({textIsEmpty: true})
    //     }
    // },
    onFieldChange: function (fieldName, e) {
        if (e.target.value.trim().length > 0) {
            this.setState({ ['' + fieldName]: false })
        } else {
            this.setState({ ['' + fieldName]: true })
        }
    },


    render: function () {
        var agreeNotChecked = this.state.agreeNotChecked;
        var authorIsEmpty = this.state.authorIsEmpty;
        var textIsEmpty = this.state.textIsEmpty;

        return (
            <form className="add cf">
                <input className="add_author"
                       type="text"
                       defaultValue=""
                       placeholder="Ваше имя"
                       ref="author"
                       onChange={this.onFieldChange.bind(this, 'authorIsEmpty')}

                />
                <textarea className="add_text"
                          defaultValue=""
                          placeholder="Текст новости"
                          ref="text"
                          onChange={this.onFieldChange.bind(this, 'textIsEmpty')}

                ></textarea>
                <label className="add_checkrule">
                    <input type="checkbox"
                           ref="checkrule"
                           onChange={this.onCheckRuleClick}
                           className="checkbox"
                    />
                    Я согласен с правилами
                </label>
                <button className="add_btn"
                        onClick={this.onButtonClickHandler}
                        ref="alert_button"
                        disabled={ agreeNotChecked || authorIsEmpty || textIsEmpty}
                >
                    Добавить новость
                </button>
            </form>

        )
    }

});

var Article = React.createClass({
    propTypes: {
        data: React.PropTypes.shape({
            author: React.PropTypes.string.isRequired,
            text: React.PropTypes.string.isRequired,
            bigText: React.PropTypes.string.isRequired,
        })
    },

    getInitialState: function () {
        return { visible: false };
    },

    readmoreClick: function (e) {
        e.preventDefault();
        this.setState({ visible: true });
    },

    render: function () {
        var author = this.props.data.author,
            text = this.props.data.text,
            bigText = this.props.data.bigText,
            visible = this.state.visible;



        return (
            <div className="article">
                <p className="news_author">{author}:</p>
                <p className="news_text">{text}</p>
                <a href="#" className={"news_readmore " + (visible ? 'none': '')} onClick={this.readmoreClick} >Подробнее</a>
                <p className={"news_big-text " + (visible ? '': 'none')}>{bigText}</p>
            </div>
        );
    }

});

var News = React.createClass({

    propTypes: {
        data: React.PropTypes.array.isRequired
    },

    render: function () {
        var data = this.props.data;

        var newsTemplate;

        if (data.length > 0) {
            newsTemplate = data.map(function (item, index) {
                return (
                    <div key={index}>
                        <Article data={item}/>
                    </div>
                )
            })
        } else {
            newsTemplate = <p>К сожалению, новостей нет</p>
        }


        return (
            <div className="news">
                {newsTemplate}
                <strong className={"news_count " + (data.length > 0 ? '': 'none')}>Всего новостей: {data.length}</strong>
            </div>
        )

    }

});

var App = React.createClass({

    getInitialState: function () {
        return {
            news: my_news
        }
    },

    componentDidMount: function () {

    },

    componentWillUnmount: function () {

    },

    render: function () {
        return (
            <div className="app">
                <h3>Новости</h3>
                <Add/>
                <News data={this.state.news}/>
            </div>
        )
    }

});

ReactDOM.render(
    <App />,
    document.getElementById("root")
);